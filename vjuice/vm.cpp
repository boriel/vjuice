
#include <stdio.h>

#include "defs.h"
#include "vm.h"


#define PUSH(x)		{ set_SP(SP - sizeof(x)); memcpy((void *)&data[SP], (void *)&(x), sizeof(x)); }
#define POP(x)		{ memcpy((void *)&(x), (void *)&data[SP], sizeof(x)); set_SP(SP + sizeof(x)); }

#define GETOP(x)	{ memcpy((void *)&(x), &prog[PC], sizeof(x)); PC += sizeof(x); }
#define LOAD(dst, src)	memcpy((void *)&(dst), (void *)(&src), sizeof(src))
#define STORE(src, dst)	memcpy((void *)&(dst), (void *)(&src), sizeof(src))

// Pop byte test and check for result, stores result in F flag
#define _NZ() T_I8 _F; POP(_F);
#define _Z()  T_I8 _F; POP(_F); _F = !_F;
#define _LZ() T_I8 _F; POP(_F); _F = _F < 0;
#define _GZ() T_I8 _F; POP(_F); _F = _F > 0;
#define _GE() T_I8 _F; POP(_F); _F = _F >= 0;
#define _LE() T_I8 _F; POP(_F); _F = _F <= 0;

// Instructions
#define _JP()    { memcpy((void *)&PC, &prog[PC], sizeof(PC)); }
#define _CALL()  { PUSH(PC); _JP(); }
#define _RET()   { POP(PC); }


#define _STOR(X)    \
    T_Register ad;  \
    X N;            \
    POP(N);         \
    GETOP(ad);      \
    STORE(N, data[ad]);

#define _ISTOR(X)   \
    T_Register ad;  \
    X N;            \
    POP(N);         \
    GETOP(ad);      \
    LOAD(ad, data[ad]); \
    STORE(N, data[ad]);

#define _STORBP(X)  \
    X N;            \
    POP(N);         \
    STORE(N, data[BP]);

#define _ISTORBP(X) \
    T_Register ad;  \
    X N;            \
    POP(N);         \
    LOAD(ad, data[BP]); \
    STORE(N, data[ad]);

#define _STORBPi(X) \
    T_I32 off;      \
    X N;            \
    GETOP(off);     \
    POP(N);         \
    STORE(N, data[BP + off]);

#define _ISTORBPi(X) \
    T_I32 off;      \
    T_Register ad;  \
    X N;            \
    GETOP(off);     \
    POP(N);         \
    LOAD(ad, data[BP + off]); \
    STORE(N, data[ad]);

#define _LOAD(X)    \
	X N;            \
    GETOP(N);       \
	PUSH(N);

#define _ILOAD(X)   \
	T_Register ad;  \
    X N;            \
    GETOP(ad);      \
    LOAD(N, data[ad]); \
	PUSH(N);

#define _LOADBP(X) \
    X N;            \
    LOAD(N, data[BP]); \
	PUSH(N);

#define _LOADBPi(X) \
    T_I32 off;      \
    X N;            \
    GETOP(off);     \
    LOAD(N, data[BP + off]); \
	PUSH(N);

#define _ILOADBP(X) \
    X N;            \
    T_Register add; \
    LOAD(add, data[BP]); \
    LOAD(N, data[add]); \
	PUSH(N);

#define _ILOADBPi(X) \
    T_I32 off;      \
    X N;            \
    T_Register add; \
    GETOP(off);     \
    LOAD(add, data[BP + off]); \
    LOAD(N, data[add]); \
	PUSH(N);


#define __RET_T(X)  \
    X;              \
    if (_F)         \
        _RET;       \

#define __INSTR_T(X, F) \
    F;                  \
    if (_F) {           \
        X;              \
    } else              \
        PC += sizeof(PC);



#define __ADD__(T)	\
	T tmp1, tmp2;	\
	POP(tmp2);		\
	POP(tmp1);		\
	tmp1 += tmp2;	\
	PUSH(tmp1);

#define __SUB__(T)	\
	T tmp1, tmp2;	\
	POP(tmp2);		\
	POP(tmp1);		\
	tmp1 -= tmp2;	\
	PUSH(tmp1);

#define __MUL__(T)	\
	T tmp1, tmp2;	\
	POP(tmp2);		\
	POP(tmp1);		\
	tmp1 *= tmp2;	\
	PUSH(tmp1);

#define __DIV__(T)	\
	T tmp1, tmp2;	\
	POP(tmp2);		\
	POP(tmp1);		\
	tmp1 /= tmp2;	\
	PUSH(tmp1);

#define __NEG__(T)	\
	T tmp;          \
	POP(tmp);       \
	tmp = -tmp;     \
	PUSH(tmp);

#define __CMP__(T)	\
	T tmp1, tmp2;	\
	POP(tmp2);		\
	POP(tmp1);		\
	T_I8 result = 0;  \
    if (tmp1 < tmp2) \
        result = -1; \
    else if (tmp2 > tmp2) \
        result = 1; \
	PUSH(result);

#define __SWP__(T)  \
    T tmp1, tmp2;   \
    POP(tmp2);      \
    POP(tmp1);      \
    PUSH(tmp2);     \
    PUSH(tmp1);

#define __DUP__(T)  \
    T tmp;          \
    POP(tmp);       \
    PUSH(tmp);      \
    PUSH(tmp);


typedef void (VM::* T_OpcodeFunc)(void);

// Vector to execute instructions
static std::vector<T_OpcodeFunc> EXEC;


VM::VM()
{
	PC = 0;
	SP = 0;
	BP = 0;

	EXEC.resize(__LAST_OP__, &VM::HALT);

	EXEC[_NOP]		= &VM::NOP;
	EXEC[_HALT]		= &VM::HALT;
	EXEC[_MCPY]		= &VM::MCPY;
	EXEC[_INCSP8]	= &VM::INCSP8;
	EXEC[_DECSP8]	= &VM::DECSP8;
	EXEC[_INCBP8]	= &VM::INCBP8;
	EXEC[_DECBP8]	= &VM::DECBP8;
	EXEC[_SWP8]		= &VM::SWP8;
	EXEC[_SWP32]	= &VM::SWP32;
	EXEC[_SWP64]	= &VM::SWP64;
	EXEC[_JP]		= &VM::JP;
	EXEC[_JPBP]		= &VM::JPBP;
	EXEC[_JPIBP]	= &VM::JPIBP;
	EXEC[_JPIBPi]	= &VM::JPIBPi;
	EXEC[_CALL]		= &VM::CALL;
	EXEC[_CALLBP]	= &VM::CALLBP;
	EXEC[_CALLIBP]	= &VM::CALLIBP;
	EXEC[_CALLIBPi]	= &VM::CALLIBPi;
	EXEC[_RET]		= &VM::RET;
	EXEC[_DUP8]		= &VM::DUP8;
	EXEC[_DUP32]	= &VM::DUP32;
	EXEC[_DUP64]	= &VM::DUP64;
	EXEC[_JP_Z]		= &VM::JP_Z;
	EXEC[_CALL_Z]	= &VM::CALL_Z;
	EXEC[_RET_Z]	= &VM::RET_Z;
	EXEC[_JP_NZ]	= &VM::JP_NZ;
	EXEC[_CALL_NZ]	= &VM::CALL_NZ;
	EXEC[_RET_NZ]	= &VM::RET_NZ;
	EXEC[_JP_GZ]	= &VM::JP_GZ;
	EXEC[_CALL_GZ]	= &VM::CALL_GZ;
	EXEC[_RET_GZ]	= &VM::RET_GZ;
	EXEC[_JP_LZ]	= &VM::JP_LZ;
	EXEC[_CALL_LZ]	= &VM::CALL_LZ;
	EXEC[_RET_LZ]	= &VM::RET_LZ;
	EXEC[_CALL_GE]	= &VM::CALL_GE;
	EXEC[_RET_GE]	= &VM::RET_GE;
	EXEC[_JP_LE]	= &VM::JP_LE;
	EXEC[_CALL_LE]	= &VM::CALL_LE;
	EXEC[_RET_LE]	= &VM::RET_LE;
    EXEC[_ADDI8]    = &VM::ADD8;
    EXEC[_ADDU8]    = &VM::ADD8;
	EXEC[_ADDI32]	= &VM::ADD32;
	EXEC[_ADDU32]	= &VM::ADD32;
	EXEC[_ADDF]		= &VM::ADDF;
	EXEC[_ADDD]		= &VM::ADDD;
    EXEC[_SUBI8]    = &VM::SUB8;
    EXEC[_SUBU8]    = &VM::SUB8;
	EXEC[_SUBI32]	= &VM::SUB32;
	EXEC[_SUBU32]	= &VM::SUB32;
	EXEC[_SUBF]		= &VM::SUBF;
	EXEC[_SUBD]		= &VM::SUBD;
    EXEC[_MULI8]    = &VM::MULI8;
    EXEC[_MULU8]    = &VM::MULU8;
	EXEC[_MULI32]	= &VM::MULI32;
	EXEC[_MULU32]	= &VM::MULU32;
	EXEC[_MULF]		= &VM::MULF;
	EXEC[_MULD]		= &VM::MULD;
	EXEC[_DIVI32]	= &VM::DIVI32;
	EXEC[_DIVU32]	= &VM::DIVU32;
	EXEC[_DIVF]		= &VM::DIVF;
	EXEC[_DIVD]		= &VM::DIVD;
    EXEC[_NEGI8]    = &VM::NEG8;
	EXEC[_NEGI32]	= &VM::NEG32;
	EXEC[_NEGF]		= &VM::NEGF;
	EXEC[_NEGD]		= &VM::NEGD;
    
    EXEC[_LOAD8]    = &VM::LOAD8;   // Push 8  bits into the stack
	EXEC[_LOAD32]	= &VM::LOAD32;  // Push 32 bits into the stack
	EXEC[_LOAD64]	= &VM::LOAD64;  // Push 64 bits into the stack
    EXEC[_ILOAD8]   = &VM::ILOAD8;
    EXEC[_ILOAD32]  = &VM::ILOAD32;
    EXEC[_ILOAD64]  = &VM::ILOAD64;
    
    EXEC[_LOADBP8]  = &VM::LOADBP8;   // Push 8  bits into the stack
	EXEC[_LOADBP32]	= &VM::LOADBP32;  // Push 32 bits into the stack
	EXEC[_LOADBP64]	= &VM::LOADBP64;  // Push 64 bits into the stack
    EXEC[_LOADBPi8] = &VM::LOADBPi8;
    EXEC[_LOADBPi32]= &VM::LOADBPi32;
    EXEC[_LOADBPi64]= &VM::LOADBPi64;
    
    EXEC[_ILOADBP8]   = &VM::ILOADBP8;   // Push 8  bits into the stack
	EXEC[_ILOADBP32]  = &VM::ILOADBP32;  // Push 32 bits into the stack
	EXEC[_ILOADBP64]  = &VM::ILOADBP64;  // Push 64 bits into the stack
    EXEC[_ILOADBPi8]  = &VM::ILOADBPi8;
    EXEC[_ILOADBPi32] = &VM::ILOADBPi32;
    EXEC[_ILOADBPi64] = &VM::ILOADBPi64;
    
    EXEC[_STOR8]    = &VM::STOR8;
    EXEC[_STOR32]   = &VM::STOR32;
    EXEC[_STOR64]   = &VM::STOR64;
    EXEC[_ISTOR8]   = &VM::ISTOR8;
    EXEC[_ISTOR32]  = &VM::ISTOR32;
    EXEC[_ISTOR64]  = &VM::ISTOR64;

    EXEC[_STORBP8]    = &VM::STORBP8;
    EXEC[_STORBP32]   = &VM::STORBP32;
    EXEC[_STORBP64]   = &VM::STORBP64;
    EXEC[_ISTORBP8]   = &VM::ISTORBP8;
    EXEC[_ISTORBP32]  = &VM::ISTORBP32;
    EXEC[_ISTORBP64]  = &VM::ISTORBP64;

    EXEC[_STORBPi8]    = &VM::STORBPi8;
    EXEC[_STORBPi32]   = &VM::STORBPi32;
    EXEC[_STORBPi64]   = &VM::STORBPi64;
    EXEC[_ISTORBPi8]   = &VM::ISTORBPi8;
    EXEC[_ISTORBPi32]  = &VM::ISTORBPi32;
    EXEC[_ISTORBPi64]  = &VM::ISTORBPi64;
        
    EXEC[_CMPI8]    = &VM::CMPI8;
	EXEC[_CMPU8]    = &VM::CMPU8;
	EXEC[_CMPI32]   = &VM::CMPI32;
    EXEC[_CMPU32]   = &VM::CMPU32;
    EXEC[_CMPF]     = &VM::CMPF;
    EXEC[_CMPD]     = &VM::CMPD;
	EXEC[_LDSP]		= &VM::LDSP;
	EXEC[_LDBP]		= &VM::LDBP;
	EXEC[_ILDSP]	= &VM::ILDSP;
	EXEC[_ILDBP]	= &VM::ILDBP;
	EXEC[_STSP]		= &VM::STSP;
	EXEC[_STBP]		= &VM::STBP;
	EXEC[_LDBPSP]	= &VM::LDBPSP;
	EXEC[_LDSPBP]	= &VM::LDSPBP;
	EXEC[_PUSHBP]	= &VM::PUSHBP;
	EXEC[_POPBP]	= &VM::POPBP;
	EXEC[_COUT]		= &VM::COUT;
    EXEC[_COUTI]    = &VM::COUTI;
    EXEC[_COUTD]    = &VM::COUTD;
	EXEC[_CIN]		= &VM::CIN;
    EXEC[_CINI]		= &VM::CINI;
    EXEC[_CIND]		= &VM::CIND;

	Cont = true;

    /*
 	std::vector<T_Opcode> prg;
	prg.push_back(_LDSP);
	prg.push_back(T_Opcode(0x00));
	prg.push_back(T_Opcode(0x40));
	prg.push_back(T_Opcode(0x00));
	prg.push_back(T_Opcode(0x00));
    prg.push_back(_LOAD32);
	prg.push_back(T_Opcode('r'));
	prg.push_back(T_Opcode('l'));
	prg.push_back(T_Opcode('d'));
    prg.push_back(T_Opcode('!'));
	prg.push_back(_LOAD32);
	prg.push_back(T_Opcode('o'));
	prg.push_back(T_Opcode(' '));
	prg.push_back(T_Opcode('w'));
    prg.push_back(T_Opcode('o'));
	prg.push_back(_LOAD32);
	prg.push_back(T_Opcode('H'));
	prg.push_back(T_Opcode('e'));
	prg.push_back(T_Opcode('l'));
    prg.push_back(T_Opcode('l'));
    prg.push_back(_LDBPSP);
	prg.push_back(_COUTI);
    prg.push_back(_COUTI);
    prg.push_back(_COUTI);
	prg.push_back(_COUTI);
	prg.push_back(_COUTI);
    prg.push_back(_COUTI);
    prg.push_back(_COUTI);
	prg.push_back(_COUTI);
	prg.push_back(_COUTI);
    prg.push_back(_COUTI);
    prg.push_back(_COUTI);
	prg.push_back(_COUTI);
    prg.push_back(_HALT);

	load(prg);
    save("test.vj");
    
	//exec();
    */
}


void VM::set_SP(const T_Register &val)
{
	SP = val;
	if (SP < 0) 
		SP = data.size();
	else if (SP > data.size())
		data.resize(SP + 1 + (SP >> 1), 0);
}


void VM::load(const std::vector<T_Opcode> &prg)
{
	prog.resize(prg.size(), _NOP);
	for (unsigned i = 0; i < prg.size(); ++i)
		prog[i] = prg[i];
}


void VM::load(const std::string &fName)
{
    FILE *f = fopen(fName.c_str(), "rb");
    if (f == NULL)
        return;

    fseek(f, 0, SEEK_END);
    long fSize = ftell(f);
    
    prog.resize(fSize, _NOP);
    fseek(f, 0, SEEK_SET);
    fread((void *)&prog[0], sizeof(prog[0]), prog.size(), f);
    fclose(f);
}


void VM::save(const std::string &fName)
{
    FILE *f = fopen(fName.c_str(), "wb");
    if (f == NULL)
        return;
    fwrite((void *)&prog[0], sizeof(prog[0]), prog.size(), f);
    fclose(f);
}


void VM::exec()
{
	while (Cont && PC < prog.size()) {
		instr = (T_Opcode)prog[PC];
		PC++;
		decode(instr);
	}
}


void VM::decode(const T_Opcode &i)
{
	(this->*EXEC[i])();
}


// ---------------------------------------------
// Instructions
// ---------------------------------------------
void VM::NOP()
{
}


void VM::HALT()
{
	Cont = false;
}


void VM::MCPY()
{
	T_Register dst, src;
	T_U32 N;

	POP(N);
	POP(src);
	POP(dst);

	memcpy((void *)&data[dst], (void *)&data[src], N);
}


void VM::INCSP8()
{
	set_SP(SP + 1);
}


void VM::DECSP8()
{
	set_SP(SP - 1);
}


void VM::INCBP8()
{
	BP++;
}


void VM::DECBP8()
{
	BP--;
}


void VM::SWP8()
{
    __SWP__(T_U8);
}

void VM::SWP32()
{
    __SWP__(T_U32);
}

void VM::SWP64()
{
    __SWP__(T_D);
}


void VM::JP()
{
	GETOP(PC);
}

void VM::JPBP()
{
	PC = BP;
}

void VM::JPIBP()
{
	LOAD(PC, data[BP]);
}

void VM::JPIBPi()
{
	T_Register tmp;

	GETOP(tmp);
	LOAD(PC, data[BP + tmp]);
}


void VM::CALL()
{
    _CALL();
}

void VM::CALLBP()
{
	PUSH(PC);
	JPBP();
}

void VM::CALLIBP()
{
	PUSH(PC);
	JPIBP();
}

void VM::CALLIBPi()
{
	PUSH(PC);
	JPIBPi();
}


void VM::RET()
{
	POP(PC);
}


void VM::DUP8()
{
    __DUP__(T_U8);
}

void VM::DUP32()
{
    __DUP__(T_U32);
}

void VM::DUP64()
{
    __DUP__(T_D);
}


void VM::JP_Z()
{
    __INSTR_T(_JP(), _Z());
}

void VM::CALL_Z()
{
    __INSTR_T(_CALL(), _Z());
}

void VM::RET_Z()
{
    __RET_T(_Z());
}


void VM::JP_NZ()
{
    __INSTR_T(_JP(), _NZ());
}

void VM::CALL_NZ()
{
    __INSTR_T(_CALL(), _NZ());
}

void VM::RET_NZ()
{
    __RET_T(_NZ());
}


void VM::JP_GZ()
{
    __INSTR_T(_JP(), _GZ());
}

void VM::CALL_GZ()
{
    __INSTR_T(_CALL(), _GZ());
}

void VM::RET_GZ()
{
    __RET_T(_GZ());
}


void VM::JP_LZ()
{
    __INSTR_T(_JP(), _LZ());
}

void VM::CALL_LZ()
{
    __INSTR_T(_CALL(), _LZ());
}

void VM::RET_LZ()
{
    __RET_T(_LZ());
}


void VM::JP_GE()
{
    __INSTR_T(_JP(), _GE());
}

void VM::CALL_GE()
{
    __INSTR_T(_CALL(), _GE());
}

void VM::RET_GE()
{
    __RET_T(_GE());
}


void VM::JP_LE()
{
    __INSTR_T(_JP(), _LE());
}

void VM::CALL_LE()
{
    __INSTR_T(_CALL(), _LE());
}

void VM::RET_LE()
{
    __RET_T(_LE());
}


void VM::ADD8()
{
    __ADD__(T_U8);
}

void VM::ADD32()
{
	__ADD__(T_U32);
}

void VM::ADDF()
{
	__ADD__(T_F);
}

void VM::ADDD()
{
	__ADD__(T_D);
}


void VM::SUB8()
{
    __SUB__(T_U8);
}

void VM::SUB32()
{
	__SUB__(T_U32);
}

void VM::SUBF()
{
	__SUB__(T_F);
}

void VM::SUBD()
{
	__SUB__(T_D);
}


void VM::MULI8()
{
    __MUL__(T_I8);
}

void VM::MULU8()
{
    __MUL__(T_U8);
}

void VM::MULI32()
{
	__MUL__(T_I32);
}

void VM::MULU32()
{
	__MUL__(T_U32);
}

void VM::MULF()
{
	__MUL__(T_F);
}

void VM::MULD()
{
	__MUL__(T_D);
}


void VM::DIVI8()
{
    __DIV__(T_I8);
}

void VM::DIVU8()
{
    __DIV__(T_U8);
}

void VM::DIVI32()
{
	__DIV__(T_I32);
}

void VM::DIVU32()
{
	__DIV__(T_U32);
}

void VM::DIVF()
{
	__DIV__(T_F);
}

void VM::DIVD()
{
	__DIV__(T_D);
}


void VM::NEG8()
{
    __NEG__(T_I8);
}

void VM::NEG32()
{
    __NEG__(T_I32);
}

void VM::NEGF()
{
    __NEG__(T_F);
}

void VM::NEGD()
{
    __NEG__(T_D);
}


void VM::LOAD8()
{
    _LOAD(T_U8);
}

void VM::LOAD32()
{
    _LOAD(T_U32);
}

void VM::LOAD64()
{
    _LOAD(T_D);
}

void VM::ILOAD8()
{
    _ILOAD(T_U8);
}

void VM::ILOAD32()
{
    _ILOAD(T_U32);
}

void VM::ILOAD64()
{
    _ILOAD(T_D);
}

void VM::LOADBP8()
{
    _LOADBP(T_U8);
}

void VM::LOADBP32()
{
    _LOADBP(T_U32);
}

void VM::LOADBP64()
{
    _LOADBP(T_D);
}

void VM::LOADBPi8()
{
    _LOADBPi(T_U8);
}

void VM::LOADBPi32()
{
    _LOADBPi(T_U32);
}

void VM::LOADBPi64()
{
    _LOADBPi(T_D);
}

void VM::ILOADBP8()
{
    _ILOADBP(T_U8);
}

void VM::ILOADBP32()
{
    _ILOADBP(T_U32);
}

void VM::ILOADBP64()
{
    _ILOADBP(T_D);
}

void VM::ILOADBPi8()
{
    _ILOADBPi(T_U8);
}

void VM::ILOADBPi32()
{
    _ILOADBPi(T_U32);
}

void VM::ILOADBPi64()
{
    _ILOADBPi(T_D);
}


void VM::STOR8()
{
    _STOR(T_U8);
}

void VM::STOR32()
{
    _STOR(T_U32);
}

void VM::STOR64()
{
    _STOR(T_D);
}

void VM::ISTOR8()
{
    _ISTOR(T_U8);
}

void VM::ISTOR32()
{
    _ISTOR(T_U32);
}

void VM::ISTOR64()
{
    _ISTOR(T_D);
}

void VM::STORBP8()
{
    _STORBP(T_U8);
}

void VM::STORBP32()
{
    _STORBP(T_U32);
}

void VM::STORBP64()
{
    _STORBP(T_D);
}

void VM::ISTORBP8()
{
    _ISTORBP(T_U8);
}

void VM::ISTORBP32()
{
    _ISTORBP(T_U32);
}

void VM::ISTORBP64()
{
    _ISTORBP(T_D);
}


void VM::STORBPi8()
{
    _STORBPi(T_U8);
}

void VM::STORBPi32()
{
    _STORBPi(T_U32);
}

void VM::STORBPi64()
{
    _STORBPi(T_D);
}

void VM::ISTORBPi8()
{
    _ISTORBPi(T_U8);
}

void VM::ISTORBPi32()
{
    _ISTORBPi(T_U32);
}

void VM::ISTORBPi64()
{
    _ISTORBPi(T_D);
}


void VM::LDSP()
{
	GETOP(SP);
	set_SP(SP);
}

void VM::LDBP()
{
	GETOP(BP);
}

void VM::ILDSP()
{
	T_Register tmp;
	
	GETOP(tmp);
	LOAD(SP, data[tmp]);
}

void VM::ILDBP()
{
	T_Register tmp;
	
	GETOP(tmp);
	LOAD(BP, data[tmp]);
}


void VM::CMPI8()
{
    __CMP__(T_I8);
}

void VM::CMPU8()
{
    __CMP__(T_U8);
}

void VM::CMPI32()
{
	__CMP__(T_I32);
}

void VM::CMPU32()
{
	__CMP__(T_U32);
}

void VM::CMPF()
{
	__CMP__(T_F);
}

void VM::CMPD()
{
	__CMP__(T_D);
}


//LD [NNNN], SP
void VM::STSP()
{
	T_Register tmp;

	GETOP(tmp);
	STORE(SP, data[tmp]);
}
//LD [NNNN], BP
void VM::STBP()
{
	T_Register tmp;

	GETOP(tmp);
	STORE(BP, data[tmp]);
}

void VM::LDBPSP()
{
	BP = SP;
}

void VM::LDSPBP()
{
	set_SP(BP);
}

void VM::PUSHBP()
{
	PUSH(BP);
}

void VM::POPBP()
{
	POP(BP);
}




void VM::COUT()
{
	T_U8 c;
	c = data[BP];
	putc(c, stdout);
}

void VM::COUTI()
{
    COUT();
    INCBP8();
}

void VM::COUTD()
{
    COUT();
    DECBP8();
}

void VM::CIN()
{
	T_U8 c;
	c = getc(stdin);
	data[BP] = c;
}

void VM::CINI()
{
    CIN();
    INCBP8();
}

void VM::CIND()
{
    CIN();
    DECBP8();
}

