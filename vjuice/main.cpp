

#include <stdlib.h>

#include "defs.h"
#include "vm.h"




int main(int argc, char *argv[])
{
	VM vm;

    if (argc < 2) {
        fprintf(stderr, "Usage: %s <program.vj>\n", argv[0]);
       return EXIT_FAILURE;
    }

    vm.load(argv[1]);
    vm.exec();

	return EXIT_SUCCESS;
}

