
#ifndef __DEFS_H__
#define __DEFS_H__



typedef enum {
    _NOP, // NO OP
    _HALT, // HALT
    _SRV, // SERVICE
    _MCPY, // COPY ADDR1, ADDR2, N <bytes>

    _SWP8,
    _SWP32,
    _SWP64,
    
    _DUP8,
    _DUP32,
    _DUP64,

    _JP,
    _JPBP, // JP BP
    _JPIBP, // JP [BP]
    _JPIBPi, // JP [BP + i32]

    _JP_Z,
    _JP_NZ,
    _JP_GZ,
    _JP_LZ,
    _JP_GE, // Less or equal to 0
    _JP_LE, // Greater or equal to 0

    _CALL,
    _CALLBP, // CALL BP
    _CALLIBP, // CALL [BP]
    _CALLIBPi, // CALL [BP + i32]

    _CALL_Z,
    _CALL_NZ,
    _CALL_GZ,
    _CALL_LZ,
    _CALL_GE, // Less or equal to 0
    _CALL_LE, // Greater or equal to 0
        
    _RET,
    _RET_Z,
    _RET_NZ,
    _RET_GZ,
    _RET_LZ,
    _RET_GE,
    _RET_LE,

    _ADDI8,
    _ADDU8,
    _ADDI32,
    _ADDU32,
    _ADDF,
    _ADDD,

    _SUBI8,
    _SUBU8,
    _SUBI32,
    _SUBU32,
    _SUBF,
    _SUBD,

    _MULI8,
    _MULU8,
    _MULI32,
    _MULU32,
    _MULF,
    _MULD,

    _DIVI8,
    _DIVU8,
    _DIVI32,
    _DIVU32,
    _DIVF,
    _DIVD,
    
    _NEGI8,
    _NEGI32,
    _NEGF,
    _NEGD,

    _INCSP8,
    _INCSP32,
    _INCSP64,

    _DECSP8,
    _DECSP32,
    _DECSP64,

    _INCBP8,
    _INCBP32,
    _INCBP64,

    _DECBP8,
    _DECBP32,
    _DECBP64,

    _INCISP8,  // [SP]++ // Increments byte value
    _INCISP32, // [SP] += 4 // Increments byte value
    _INCISP64, // [SP] += 8

    _DECISP8,  // [SP]++ // Increments byte value
    _DECISP32, // [SP] += 4 // Increments byte value
    _DECISP64, // [SP] += 8

    _INCIBP,   // [BP]++
    _INCIBP32, // [BP] += 4 // Increments byte value
    _INCIBP64, // [BP] += 8

    _DECIBP,   // [BP]++
    _DECIBP32, // [BP] += 4 // Increments byte value
    _DECIBP64, // [BP] += 8
    
    _LOAD8,
    _LOAD32,
    _LOAD64,
    _ILOAD8,   // PUSH (8 bits)[BP]
    _ILOAD32,
    _ILOAD64,

    _LOADBP8,
    _LOADBP32,
    _LOADBP64,
    _LOADBPi8,
    _LOADBPi32,
    _LOADBPi64,

    _ILOADBP8,
    _ILOADBP32,
    _ILOADBP64,
    _ILOADBPi8,
    _ILOADBPi32,
    _ILOADBPi64,

    _STOR8,
    _STOR32,
    _STOR64,
    _ISTOR8,
    _ISTOR32,
    _ISTOR64,

    _STORBP8,
    _STORBP32,
    _STORBP64,
    _ISTORBP8,
    _ISTORBP32,
    _ISTORBP64,

    _STORBPi8,
    _STORBPi32,
    _STORBPi64,
    _ISTORBPi8,
    _ISTORBPi32,
    _ISTORBPi64,

    _CMPI8,
    _CMPU8,
    _CMPI32, // LEAVES 0  (i32) if A 
    _CMPU32, // LEAVES -1 (i32) (A < B)
    _CMPF, // LEAVES 1  (i32) if A > B
    _CMPD,

    _LDSP, // LOAD SP, NNNN    
    _LDBP, // LOAD BP, NNNN    
    _ILDSP, // LOAD SP, [NNNN]    
    _ILDBP, // LOAD BP, [NNNN]
    
    _STSP, // STORE SP, [NNNN]
    _STBP, // STORE BP, [NNNN]
    _LDBPSP, // LD BP, SP
    _LDSPBP, // LD SP, BP

    _PUSHBP, // PUSH BP
    _POPBP, // POP BP
    _IPUSHBP, // PUSH [BP]
    _IPOPBP, // POP [BP] 

    _IPUSHBPi, // PUSH [BP + i32] 
    _IPOPBPi, // POP  [BP + i32] 
    
    _COUT, // Outputs byte pointed by BP to console
    _COUTI, // COUT; BP++
    _COUTD, // COUT; BP--
    _CIN, // Input lower byte from console [BP] << cin;
    _CINI, // CIN; BP++;
    _CIND, // CIN; BP--
    __LAST_OP__
} T_Opcode;



#endif
