#ifndef __VM_H__
#define __VM_H__

#include <vector>
#include <string.h>
#include "defs.h"

typedef unsigned char	T_Cell;
typedef unsigned int	T_Register;

typedef unsigned char	T_I8;
typedef unsigned char	T_U8;
typedef int				T_I32;
typedef unsigned		T_U32;
typedef	float			T_F; // Float (32 bits)
typedef	double			T_D; // Long double (64 bits)



class VM {
private:
	std::vector<T_Cell> data; // Data Memory
	std::vector<T_Cell> prog; // Program Memory
	
	bool Cont; // To false to HALT the machine
	T_Opcode instr; // Current instruction

public:
	T_Register PC;
	T_Register SP;
	T_Register BP;

	inline void set_SP(const T_Register &val);

	VM();

	void exec();
	void load(const std::vector<T_Opcode> &prg); // Loads a program into memory
    void load(const std::string &fName);
    void save(const std::string &fName);
	inline void decode(const T_Opcode &i);

	// Instruction implementation
	void NOP();
	void HALT();
	void MCPY();

	void INCSP8();
	void DECSP8();
	void INCBP8();
	void DECBP8();

	void SWP8();
	void SWP32();
	void SWP64();
	
	void JP();
	void JPBP();
	void JPIBP();
	void JPIBPi();

	void CALL();
	void CALLBP();
	void CALLIBP();
	void CALLIBPi();
	
	void RET();

	void DUP8();
	void DUP32();
	void DUP64();

	void JP_Z();
	void CALL_Z();
	void RET_Z();

	void JP_NZ();
	void CALL_NZ();
	void RET_NZ();
		
	void JP_GZ();
	void CALL_GZ();
	void RET_GZ();

	void JP_LZ();
	void CALL_LZ();
	void RET_LZ();

	void JP_GE();
	void CALL_GE();
	void RET_GE();

	void JP_LE();
	void CALL_LE();
	void RET_LE();

    void ADD8();
	void ADD32();
	void ADDF();
	void ADDD();

    void SUB8();
	void SUB32();
	void SUBF();
	void SUBD();

    void MULI8();
    void MULU8();
	void MULI32();
	void MULU32();
	void MULF();
	void MULD();
	
    void DIVI8();
    void DIVU8();
	void DIVI32();
	void DIVU32();
	void DIVF();
	void DIVD();
        	
	void CMPI8();
	void CMPU8();
	void CMPI32();
	void CMPU32();
	void CMPF();
	void CMPD();

    void NEG8();
	void NEG32();
	void NEGF();
	void NEGD();

    void LOAD8();
	void LOAD32();
	void LOAD64();
    void ILOAD8();  // PUSH [NNNN] (8 bits)
    void ILOAD32();
    void ILOAD64();

    void LOADBP8(); // PUSH (char) [BP]
    void LOADBP32();
    void LOADBP64();
    void LOADBPi8();  // PUSH ( i8) [BP + i32]
    void LOADBPi32(); // PUSH (i32) [BP + i32]
    void LOADBPi64(); // PUSH (i64) [BP + i32]

    void ILOADBP8();   // PUSH (char) [[BP]]
    void ILOADBP32();
    void ILOADBP64();
    void ILOADBPi8();  // PUSH ( i8) [[BP + i32]]
    void ILOADBPi32(); // PUSH (i32) [[BP + i32]]
    void ILOADBPi64(); // PUSH (i64) [[BP + i32]]

    void STOR8();
    void STOR32();
    void STOR64();
    void ISTOR8();
    void ISTOR32();
    void ISTOR64();

    void STORBP8();
    void STORBP32();
    void STORBP64();
    void ISTORBP8();
    void ISTORBP32();
    void ISTORBP64();

    void STORBPi8();
    void STORBPi32();
    void STORBPi64();
    void ISTORBPi8();
    void ISTORBPi32();
    void ISTORBPi64();

	void LDSP();    // LD SP, NNNN
	void LDBP();    // LD BP, NNNN
	void ILDSP();   // LD SP, [NNNN]
	void ILDBP();   // LD BP, [NNNN]

	void STSP();    // LD [NNNN], SP
	void STBP();    // LD [NNNN], BP
	void LDBPSP();  // BP <- SP
	void LDSPBP();  // SP <- BP

	void PUSHBP();
	void POPBP();

	void COUT();  // Ouput [BP] byte to stdout (or another file)
    void COUTI(); // COUT; BP++
    void COUTD(); // COUT; BP--

	void CIN();
    void CINI();
    void CIND();
};


#endif